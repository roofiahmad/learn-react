import React, { Component } from 'react'
import "./css/delapan.css"

class Delapan extends Component {
    render() {
        return (
            <div>
                <div className="card">
                    <h1>Title - Card 1</h1>
                    <p>Medium length description. Let's add a few more words here.</p>
                    <div className="visual"></div>
                </div>
                <div className="card">
                    <h1>Title - Card 2</h1>
                    <p>Long Description. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sed est error repellat veritatis.</p>
                    <div className="visual"></div>
                </div>
                <div className="card">
                    <h1>Title - Card 3</h1>
                    <p>Short Description.</p>
                    <div className="visual"></div>
                </div>
            </div>
        )
    }
}

export default Delapan;